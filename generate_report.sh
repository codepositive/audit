#!/bin/bash 
  DIR=${1:-"."}
  START="<pre>"
  END="</pre>"
  H1="---+"
  H2="---++"
  HR="------------------------------"
 
usage(){
  echo "Generates a text report from Drupal scan data."
  echo
  echo "usage: $0 <Audit Data Dir>"
  echo
  echo "   Audit data dir: Location of output from drupal_scan"
  echo "                   Defaults to current dir"
  echo
  exit 1
}


auditdata(){
  echo "Unable to detect Drupal scan data"
  echo "Directory: $DIR"
  echo
  usage 
}


p(){
  TITLE="$1"
  INFILE="$DIR/$2"  
  echo $H2 $TITLE 
  echo $START 
  cat $INFILE 
  echo $END 
}

report(){
  echo $H1 `cat $DIR/DRUPAL_SCAN`
  echo 
  echo $HR
  p "DRUSH STATUS" status 
  p "QUICK STATS" stats 
  echo $HR
  p "MODULES" updates
  echo $HR
  p "HACKED REPORT" hacked_report 
  echo $HR
  p "FEATURES" features
  p "PANELS" panels
  echo $HR
  p "ROLES" roles
  p "CUSTOM MENUS" custom_menus
  p "FIELD TYPES" field_types
  p "NODE TYPES" node_types
  echo $HR
  p "CUSTOM MODULES" custom_modules
  echo $HR
  p "CODER SQL" coder_sql
  p "CODER SECURITY REPORT" coder_security
  echo $HR
  p "PROD CHECK" prod_check
 
}

# Make sure that we have been given drupal scan output
[ -e "$DIR/DRUPAL_SCAN" ] || auditdata

report

