#!/bin/bash 

###############################################
#### Declarations

# Where to look for custom modules.
CUSTOM_MODULE_DIR="sites/all/modules/custom"

#################################################
#### Functions

usage() {
  echo "Creates an audit data dump for a Drupal site. "
  echo "Must be run from the Drupal root."
  echo 
  echo "This script will install and enable some modules."
  echo "It is advisable to run this on a staging server."
  echo  
  echo "Usage: "
  echo "   $0 <site uri> [output file]"
  echo 
  echo "  site uri: The uri of the site to be scanned. Do not include http "
  echo "            eg: www.example.com"
  echo 
  echo " output file: The name and path of the tar archive to create"
  echo "           defaults to /tmp/audit_[cleaned site uri].tar"
  exit 1
}

drush_missing() {
  echo "Unable to locate drush on the path"
  echo ""

  ## download drush to /tmp
  cd /tmp
  wget http://ftp.drupal.org/files/projects/drush-7.x-5.9.tar.gz

  echo "drush has been downloaded and placed in /tmp"
  echo "Please ensure that drush is available"
  echo
  exit 1
}


# Create an archive of all created files
archive(){
#  ARCHIVE="/tmp/audit_$CLEAN_URI.tar"
  [ -e $ARCHIVE ] && rm $ARCHIVE
  tar -cf $ARCHIVE --directory /tmp $DIR_NAME 
  if [[ "$?" ]];
  then
    echo "Audit data written to: $ARCHIVE"
  else 
   echo "Unable to create audit archive file"
   echo "Attempted to create : $ARCHIVE"
   echo 
  fi
}


# wrapped drush command to manage standard parameters
d(){
  OUTPUT=$1
  COMMAND=$2
  shift 2 

  drush --pipe --uri="http://$URI" $COMMAND $@ &> $DIR/$OUTPUT
} 

# wrapper for managing sqlq drush command
# output is returned to stdout to allow for concatenated files
sqlq() {
  SQL=$1  
  shift 1

  drush --uri="http://$URI" sqlq "$SQL" $@  

}

stat() {
  NAME=$1
  SQL=$2
  VALUE=`drush --uri="http://$URI" sqlq "$SQL" | tr "\n" ":"`
  echo "${NAME}:${VALUE}" >> $DIR/stats
}
#################################################################
#### INITIALIZATION

# Make sure that we are in a drupal document root
[ -e "index.php" ] || usage

# Make sure that drush exists
DRUSH=`which drush`
[ "$DRUSH" ] || drush_missing

# test that we have been provided wiht a uri
URI="$1"
[ "$1" ] || usage 
CLEAN_URI=`echo $URI | tr -cs [:alnum:] _`

# create data directory
DIR_NAME="audit_$CLEAN_URI"
DIR="/tmp/$DIR_NAME"
[ -e $DIR ] || mkdir $DIR

ARCHIVE=${2:-"/tmp/audit_$CLEAN_URI.tar"}
DRUPAL_FILE_DIR=`drush st | grep "File direct" | cut -d ":" -f 2 | tr -d " "`

#####
echo "Drupal site data for: $URI" > $DIR/DRUPAL_SCAN
echo "Generated: " `date` >> $DIR/DRUPAL_SCAN

##################################################################
#### DATA EXTRACTION

echo "Setting up required modules.... "

drush  --uri="http://$URI" dl -y coder
drush  --uri="http://$URI" dl -y hacked
drush  --uri="http://$URI" dl -y prod_check

drush  --uri="http://$URI" en -y  update
drush  --uri="http://$URI" en -y coder_review
drush  --uri="http://$URI" en -y prod_check
drush  --uri="http://$URI" en -y hacked


## Files
echo "Scanning directory structure ... "
ls -lR &> $DIR/ls
du &> $DIR/du


# hope that custom modules are in the right place
echo "Looking for custom modules in $CUSTOM_MODULE_DIR"
find $CUSTOM_MODULE_DIR -type f \( -name "*.module" -o -name "*.inc" \) > $DIR/custom_modules

## Drush
echo "Analysing Drupal ... "
d status st 
d field_types field-info types
d modules pm-list  --type=module --status=enabled

echo "Looking for updates"
d security_updates up --security-only

echo "Reviewing custom code" 
d coder_security coder-review $CUSTOM_MODULE_DIR --security --no-empty
d coder_style coder-review $CUSTOM_MODULE_DIR --major --no-empty
d coder_sql coder-review $CUSTOM_MODULE_DIR --sql --major --no-empty

echo "Looking for hacked modules"
drush  --uri="http://$URI" hacked-list-projects > $DIR/hacked_report
drush  --uri="http://$URI" fl > $DIR/features
drush  --uri="http://$URI" up -n  > $DIR/updates
drush  --uri="http://$URI" st  > $DIR/status

echo "Checking for production settings"
drush  --uri="http://$URI" pchk | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g" > $DIR/prod_check

## Sql statements

echo "Handy SQL statements and stats" 
sqlq "select type, name, module, description from node_type" > $DIR/node_types
sqlq  "select * from menu_custom" > $DIR/custom_menus
sqlq "select lid,name,admin_title,admin_description,category from panels_layout" > $DIR/panels
sqlq "select name from role" > $DIR/roles
sqlq "SELECT s.schema_name, CONCAT(IFNULL(ROUND(SUM(t.data_length)/1024/1024,2),0.00),'Mb') as Data_size, CONCAT(IFNULL(ROUND(SUM(t.index_length)/1024/1024,2),0.00),'Mb') as Index_size,COUNT(table_name) total_tables FROM INFORMATION_SCHEMA.SCHEMATA s LEFT JOIN INFORMATION_SCHEMA.TABLES t ON s.schema_name = t.table_schema WHERE s.schema_name not in('mysql',"information_schema",'test') GROUP BY s.schema_name order by Data_size DESC;" > $DIR/database


## SQL counts
echo "" > $DIR/stats
echo "Modules enabled: " `wc -l $DIR/modules ` >>  $DIR/stats
HACKED=`cat $DIR/hacked_report | grep "Changed" | wc -l` # count hacked files
HACKED=`expr $HACKED - 1`
echo "Possibly hacked modules:  $HACKED"  >> $DIR/stats
echo "Files dir size: " `du $DRUPAL_FILE_DIR -sh` >> $DIR/stats
stat users "select count(uid) from users where status=1"
stat nodes "select count(nid) from node where status = 1"
stat views 'select count(vid) from views_view'
stat aliases "select count(pid) from url_alias"
stat panes "select count(pid) from panels_panel"




#################################################################  
# POST PROCESS

# create an archive 
archive





